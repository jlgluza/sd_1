#procedure Taylor(order,p)

********************************************************************************
* Calculate Taylor Series of the function DS(k+p,m?,n?) = 1/((k+p)^2-m^2)^n
* expanded in p.p -> 0, where 
* order - required order of the expansion in p.p
* p - the momentum in which the expansion is made.  
* PoCo - this symbol is introduced to COunt POwers of the momentum squared. 

b DS, `p';
.sort
s PoCo(:{2*`order'});
Keep Brackets;

id `p'.`p'^n? = `p'.`p'^n*PoCo^(2*n);
id `p'.k?!{`p'} = `p'.k*PoCo;

splitarg (`p') DS; 

repeat;

  if (match(DS(k?,p?,m?,1)));

    id DS(k?,p?,m?,1) = sum_(n1,0,{2*`order'},(-1)^n1*DS(k,m,1+n1)*(p.p*PoCo^2+2*p.k*PoCo)^n1);

  elseif (match(DS(k?,p?,m?,n?{>1})));

    id DS(k?,p?,m?,n?{>1}) = sum_(n1,0,{2*`order'},DS(k,m,n+n1)*(p.p*PoCo^2+2*p.k*PoCo)^n1
				                   *(-1)^n1*fac_(n+n1-1)*invfac_(n1)*invfac_(n-1));
  endif;

endrepeat;

.sort: Taylor Expansion;

id PoCo = 1;

#endprocedure
