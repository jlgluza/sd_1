#-
off statistics;

functions f,g;      * f f,g;
Cfunctions F,G;     * cf F,G;
Autodeclare s x;   

l f1=F(x1,...,x4)*G(x1,...,x4)+G(x1,...,x4)*F(x1,...,x4);
l f2=f(x)*g(x)+g(x)*f(x);

print;
.clear    *.sort

* .sort      - wykonuje dzialania w segmencie             
* .end       - konczy program
* .clear     - zmienic na .clear w cwicz1.frm i zobaczyc (wykonuje segmenty jak niezalezne programy)
* .store     - zachowuje globalne definicje, usuwa lokalne
*___________________________________________________________________________
**** wbudowane funkcje ****

off statistics;

#message wbudowane funkcje

s x;
l f1 = invfac_(3) +x*fac_(3);
l f2 = sin_(x)^2+cos_(0);
l f3 = sig_(-3)+2*x*sig_(2);
l f4 = max_(-1,2,3)+x*min_(-10,-100);
l f5 = mod_(7,2);

print;
.clear
*___________________________________________________________________________
*** WEKTORY I INDEKSY ***
off statistics;

Vectors u,v;         * v u,v;
Indices i,j;         * i i,j;
f f;

#message wektory i indeksy

l w1 = u(1) + v(i);
l w2 = u(i) * v(j);
l w3 = u(i) * v(i);                * przyklad tzw. kontrakcji indeksow
l w4 = v(i) * u(i);
l w5 = f(i,j) * u(i) * v(j);       * specjalny wynik dzialania

print;
.clear
*_________________________________________________________________________--
*** jak nie robic kontrakcji ***
off statistics;

#message bez kontrakcji

v u;
i i=0;
i x,y;
f f1;

l f = u(i) * u(i);
l g = f1(x,y)*u(x);    * zrobic x=0 w deklaracji i zobaczyc roznice

print f,g;
.sort
off statistics;

sum i;
print f;
.sort


l h = f1(x);
sum x,1,3,5;
print h; 
.clear
*--------------------------------------------------------------------

*********************** TENSORY ***********************************
off statistics;

tensors s,t;          * t s,t;
i i,j,k,l;

l f=s(i,k)*t(k,j)+s(i,l)*t(l,j);

print;
.sort

*sum k,l;    *zrobic sum k,1,...,3,l,3,...,6;
.clear

*---------------------------------------------------------
**** wektory do tensorow ****
off statistics;

t t;
v u,v;
i i,j,k;

l f1 = v(i)*v(j)*v(k)*v(1);
l f2 = v;
l f3 = (u.v)^2* v.v;

ToTensor v,t;

print;
.sort

l f4 = t;
ToVector t,v;
print;
.clear




