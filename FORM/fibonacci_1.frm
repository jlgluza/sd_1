* page 26 tutorial
*every Fibonacci number is the sum of the previous two. 
*The sequence 1, x, x2, x3, x4, . . . also
*has this property, provided x2 = x + 1.
Symbol x;
Local Fibonacci19 = x^18;
repeat;
id x^2 = x + 1;
endrepeat;
id x = 1;
Print;
.end