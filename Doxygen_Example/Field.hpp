/******************************************************************************
 *                                                                            *
 * file4                                                                      *
 *                                                                            *
 ******************************************************************************/

#ifndef FIELD_HPP
#define FIELD_HPP 1

#include <set>
#include <vector>
#include <string>

using namespace std;

/******************************************************************************
 *                                                                            *
 * FieldType                                                                  *
 *                                                                            *
 ******************************************************************************/

/// currently supported field types

enum FieldType
{
  Scalar,
  Ghost,
  Fermion,
  Vector,
};

const int n_field_types = 4;
const int n_anticommuting_field_types = 2;
const FieldType anticommuting_field_type[2] = {Fermion, Ghost};

/******************************************************************************
 *                                                                            *
 * Field                                                                      *
 *                                                                            *
 ******************************************************************************/

struct Vertex;

/// QFT field

struct Field
{
  string                               _identifier;

  FieldType                            _type;

  string                               _mass;

  /// the same for particle and anti-particle
  string                               _name;
  
  bool                                 _particle;
  
  const Field*                         _coupled_field;
  
  /**
   *
   * The first index is the degree of the vertex, the second is the minimum
   * number of times the vertex contains the field (multiplicity), i.e. 
   * _vertices[d][m], are all vertices of degree d, which contain the field
   * at least m times.
   *
   */

  vector<vector<set<const Vertex*> > > _vertices;
};

typedef const Field* field_pointer;

#endif
